;;; baggy.el --- a blog engine with elnode -*- lexical-binding: t -*-

;; Copyright (C) 2013  Aidan Gauland

;; Author: Aidan Gauland <aidalgol@amuri.net>
;; Keywords: hypermedia, lisp
;; Based on Skinny by Nic Ferrier

;; This program is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <http://www.gnu.org/licenses/>.

;;; Commentary:

;; Baggy is an Emacs blog engine. You can write blog posts in creole
;; and serve them as HTML, or write the posts in straight HTML.

;;; Code:

(elnode-app baggy-dir
  esxml esxml-html)

(defgroup baggy nil
  "A blog engine written with Elnode."
  :group 'applications)

(defcustom baggy-port 8090
  "The TCP port on which to serve the site."
  :type '(integer)
  :group 'baggy)

(defcustom baggy-host "localhost"
  "The interface on which to serve the site."
  :type '(string)
  :group 'baggy)

(defcustom baggy-base-url (format "http://%s:%s/"
                                   baggy-host baggy-port)
  "The base URL of the Baggy instance.

This should be set to the absolute URL of the Baggy instance as
it is seen to the outside world.

This is used in the Atom feed <link rel=\"self\" href=\"...\"/>
element, and the blog post pages <base href\"...\"/> element."
  :type '(string)
  :group 'baggy)

(defcustom baggy-feed-uuid "deadbeef-dead-beef-dead-beefdeadbeef"
  "UUID to use for the Atom feed.

This string is used for the feed's <id>, prefixed with
\"urn:uuid:\"; see RFC4287."
  :type '(string)
  :group 'baggy)

(defcustom baggy-blog-name "baggy"
  "The name of the blog."
  :type '(string)
  :group 'baggy)

(defcustom baggy-blog-author nil
  "The blog author."
  :type '(string)
  :group 'baggy)

(defcustom baggy-post-format 'creole
  "Which markup language the posts are written in.

Baggy can render blog posts written in WikiCreole, or just
insert literal HTML into the post template.  The file extention
for blog posts must match the value of this variable.  For
Creole, set this to 'creole; for HTML, set this to 'html.")

(defcustom baggy-lang "en"
  "The language code for the blog.

The code for the language in which the blog is written.  This is
used as the value for the <html> lang attribute, and the <feed>
xml:lang attribute.  See the W3C article on language tags for
available language codes:
http://www.w3.org/International/articles/language-tags/"
  :type '(string)
  :group 'baggy)

(defcustom baggy-blog-css-file-names '("blog.css")
  "The names of the CSS files to use for blog posts."
  :type '(repeat file)
  :group 'baggy)

(defconst baggy-blog-includes-dir "includes/"
  "The directory for the blog include files.

The includes files are the top, header, footer, and
bottom (`baggy-blog-top-file-name',
`baggy-blog-header-file-name', `baggy-blog-footer-file-name',
and `baggy-blog-bottom-file-name').

Must be an immediate subdirectory of `baggy-root'.")

(defconst baggy-blog-top-file-name "top.html"
  "The name of the blog top file.

This file is inserted at the top of the blog post page, before
the <article> element.

This file is searched for in `baggy-blog-includes-dir'.")

(defconst baggy-blog-header-file-name "header.html"
  "The name of the blog header file.

This file is inserted at the end of the blog post page <header>
element.

This file is searched for in `baggy-blog-includes-dir'.")

(defconst baggy-blog-footer-file-name "footer.html"
  "The name of the blog footer file.

This file is inserted at the end of the blog post page <footer>
element.

This file is searched for in `baggy-blog-includes-dir'.")

(defconst baggy-blog-bottom-file-name "bottom.html"
  "The name of the blog bottom file.

This file is inserted at the end of the blog post page, after the
<article> element.

This file is searched for in `baggy-blog-includes-dir'.")

(defcustom baggy-blog-article-class nil
  "<article> class attribute.

If set, use as the value of the class attribute for the blog post
<article>."
  :type '(string)
  :group 'baggy)

(defcustom baggy-max-posts-list 10
  "Maximum number of blog posts to list on index page."

  :type '(integer)
  :group 'baggy)

(defgroup baggy-dirs nil
  "Various directories for the Baggy blog.

All paths are relative to `baggy-root'."
  :group 'baggy)

(defcustom baggy-root baggy-dir
  "The root directory of the Baggy site.

By default, this is the directory from which Baggy was loaded.
Blog posts are in a subdirectory, specified by `baggy-blog-dir'."
  :type '(directory)
  :group 'baggy-dirs)

(defcustom baggy-blog-dir "blog/"
  "The directory for blog posts.

Must be an immediate subdirectory of `baggy-root'."
  :type '(directory)
  :group 'baggy-dirs)

(defcustom baggy-css-dir "css/"
  "The directory for CSS files.

Must be an immediate subdirectory of `baggy-root'."
  :type '(directory)
  :group 'baggy-dirs)

(defun baggy/post-meta-data (post)
  "Return corresponding meta-data file for POST file.

Takes the absolute path of a blog post, and reads the
corresponding \".sexp\" file, which should contain only a single
alist with the following fields:

title
summary -- Used for the Atom <entry> <summary> element.
author -- Just the author name, not name then email.  If this is
          absent, then the value of `baggy-blog-author' is used
          instead.
published -- Date of publication in RFC3339 format.
updated -- Date of the last update in RFC3339 format.  Should be
           later than the publication date, but this is not
           enforced.
uuid -- Used for the id of feed entries, prefixed with
        \"urn:uuid\".

See RFC4287 for details on what values are appropriate."
  (with-temp-buffer
    (save-match-data
     (insert-file-contents
      (concat (file-name-sans-extension post) ".sexp"))
     (goto-char (point-min))
     (read (current-buffer)))))

(defun baggy/post-latest-timestamp (post)
  "Return the date of the last update of POST.

Returns the 'updated field of POST's meta-data, or the 'published
field if the 'updated field is absent."
  (let ((metadata (baggy/post-meta-data post)))
    (cdr (or (assoc 'updated
                    metadata)
             (assoc 'published
                    metadata)))))

(defun baggy/list-posts ()
  "Produce the list of blog posts (file names), sorted by timestamp.

Posts are all files in \"YYYY/MM/\" subdirectories of
`baggy-blog-dir' with the extention `baggy-post-format'."
  (let ((posts '()))
    (cl-block traversal
     (cl-dolist (year-dir (directory-files (concat baggy-root baggy-blog-dir)
                                           t
                                           (rx string-start (= 4 digit) string-end)))
       (cl-dolist (month-dir (directory-files year-dir
                                              t
                                              (rx string-start (= 2 digit) string-end)))
         (setq posts (append posts
                             (directory-files month-dir
                                              t
                                              (rx-to-string `(and (one-or-more any) "." ,(symbol-name baggy-post-format) string-end) t)
                                              t))))))
   (sort
    posts
    (lambda (a b)
      (cl-flet ((post-date (post)
                           (date-to-time
                            (cdr (assoc 'published (baggy/post-meta-data post))))))
        (time-less-p
         (post-date b)
         (post-date a)))))))

(defun baggy/posts-html-list ()
  "Produce an HTML list of the posts.

Each post's title is listed, and links to the post itself.
HTML is returned as ESXML, rather than a string."
  (esxml-listify
   (mapcar
    (lambda (post)
      (let ((metadata (baggy/post-meta-data post)))
        (esxml-link
         (save-match-data
           (string-match (expand-file-name
                          (format "%s\\(%s.*\\.%s\\)"
                                  baggy-root baggy-blog-dir
                                  (symbol-name baggy-post-format)))
                         post)
           (file-name-sans-extension (match-string 1 post)))
         (cdr (assoc 'title metadata)))))
    (baggy/list-posts))))

(defun baggy/render-post (post)
  "Render a blog post as HTML.

Takes a single argument POST, which is the name of the post file.
Returns HTMLas ESXML, rather than a string."
  (let ((metadata (baggy/post-meta-data post)))
    (cl-flet ((insert-file-if-exists (file)
               (when (file-exists-p
                      (concat baggy-blog-dir baggy-blog-includes-dir
                              file))
                 `(,(with-temp-buffer
                      (save-match-data
                        (insert-file-contents
                         (concat baggy-blog-dir baggy-blog-includes-dir
                                 file))
                        (buffer-string)))))))
      `(html ((lang . ,baggy-lang))
         ,(append
           (esxml-head (with-temp-buffer
                         (save-match-data
                           (insert (cdr (assoc 'title metadata)))
                           (html2text) ;; Strip HTML tags.
                           (buffer-string)))
             '(meta ((charset . "UTF-8")))
             (meta 'author
                   (if (assoc 'author metadata)
                       (cdr (assoc 'author metadata))
                     baggy-blog-author))
             (base baggy-base-url)
            (link 'alternate "application/atom+xml" "feed.xml"
                  '((title . "site feed"))))
          (mapcar
           (lambda (css)
             (esxml-head-css-link (concat baggy-css-dir css)))
           baggy-blog-css-file-names))
        (body ()
          ,@(insert-file-if-exists baggy-blog-top-file-name)
          (article ,(when baggy-blog-article-class
                      `((class . ,baggy-blog-article-class)))
            (header ()
              ,(cdr (assoc 'title metadata))
              (br ())
              ,(let ((timestamp (cdr (assoc 'published metadata))))
                `(time ((datetime . ,timestamp))
                   ,(format-time-string "%d %B %Y, %R %Z"
                                        (date-to-time timestamp))))
              ,@(when (file-exists-p
                       (concat baggy-blog-dir baggy-blog-includes-dir
                               baggy-blog-header-file-name))
                  `((br ())
                    ,(with-temp-buffer
                       (save-match-data
                         (insert-file-contents
                          (concat baggy-blog-dir baggy-blog-includes-dir
                                  baggy-blog-header-file-name))
                         (buffer-string))))))
            ,(with-temp-buffer
               (save-match-data
                 (insert-file-contents post))
               (if (eq 'creole baggy-post-format)
                   (with-current-buffer
                       (creole-html (current-buffer) nil
                                    :do-font-lock t)
                     (buffer-string))
                 (buffer-string)))
            ,@(when (file-exists-p
                     (concat baggy-blog-dir baggy-blog-includes-dir
                             baggy-blog-footer-file-name))
                `((footer ()
                    ,(with-temp-buffer
                       (save-match-data
                         (insert-file-contents
                          (concat baggy-blog-dir baggy-blog-includes-dir
                                  baggy-blog-footer-file-name))
                         (buffer-string)))))))
          ,@(insert-file-if-exists baggy-blog-bottom-file-name))))))

(defun baggy-post (httpcon)
  "Return a blog post via HTTPCON.

If using creole, render it first."
  (let ((baggy-blog-dir (concat baggy-root baggy-blog-dir))
        (creole-image-class "creole")
        (targetfile (elnode-http-mapping httpcon 1)))
    (flet ((elnode-http-mapping (httpcon which)
            (concat targetfile "." (symbol-name baggy-post-format))))
      (elnode-docroot-for baggy-blog-dir
        with post
        on httpcon
        do
        (elnode-error "Sending blog post: %s" post)
        (elnode-http-start httpcon 200 '("Content-type" . "text/html"))
        (elnode-http-send-string httpcon "<!DOCTYPE HTML>")
        (elnode-http-return httpcon
                            (pp-esxml-to-xml (baggy/render-post post)))))))

(defun baggy/expand-index-page (file-name)
  "Expand the macros in the index page specified by FILE-NAME.

Replaces the string \"<!--{{{posts}}}-->\" in the index file with
the output of `baggy/posts-html-list'."
  (with-temp-buffer
    (save-match-data
      (insert-file-contents file-name)
      (while (search-forward "<!--{{{posts}}}-->" nil t)
        (replace-match (pp-esxml-to-xml
                        (save-match-data
                          (baggy/posts-html-list)))
                       nil t)))
    (buffer-string)))

(defun baggy-index-page (httpcon)
  "Return the index page via HTTPCON."
  (let ((page (concat baggy-root "index.html")))
    (elnode-error "Sending index page.")
    (elnode-http-start httpcon 200 '("Content-type" . "text/html"))
    (elnode-http-return httpcon (baggy/expand-index-page page))))

(defun baggy/feed ()
  "Generate an Atom feed from the most recent posts."
  (let* ((posts (baggy/list-posts))
         (last-post (car posts))
         (last-post-metadata (baggy/post-meta-data last-post)))
    (concat "<?xml version=\"1.0\"?>\n"
      (pp-esxml-to-xml
        `(feed ((xmlns . "http://www.w3.org/2005/Atom")
                (xml:lang . ,baggy-lang))
           ;; Feed metadata.
           (title () ,baggy-blog-name)
           (link ((href . ,(concat baggy-base-url baggy-blog-dir "feed.xml"))
                  (rel . "self")))
           (link ((href . "./")))
           (id () ,(concat "urn:uuid:" baggy-feed-uuid))
           (updated () ,(baggy/post-latest-timestamp last-post))
           ,@(when baggy-blog-author
               `((author ()
                   (name () ,baggy-blog-author))))
           ;; Now for the entries.
           ,@(mapcar
              (lambda (post)
                (let ((metadata (baggy/post-meta-data post)))
                  `(entry ()
                     (title ((type . "xhtml"))
                       (div ((xmlns . "http://www.w3.org/1999/xhtml"))
                         ,(cdr (assoc 'title metadata))))
                     ,(if (assoc 'author metadata)
                          `(author () ,(cdr (assoc 'author metadata)))
                        `(author ()
                           (name () ,baggy-blog-author)))
                     (link ((href . ,(save-match-data
                                       (let ((post (file-name-sans-extension post)))
                                        (string-match
                                         (rx (submatch (= 4 digit) "/" (= 2 digit) "/" (one-or-more any)) string-end)
                                         post)
                                        (match-string 1 post))))))
                     (id () ,(concat "urn:uuid:"
                                     (cdr (assoc 'uuid metadata))))
                     (updated () ,(baggy/post-latest-timestamp post))
                     (summary ((type . "xhtml"))
                       (div ((xmlns . "http://www.w3.org/1999/xhtml"))
                         ,(cdr (assoc 'summary metadata)))))))
              posts))))))

(defun baggy-feed (httpcon)
  "Return a blog feed via HTTPCON.

Calls `baggy/feed' to generate the feed."
  (elnode-http-start httpcon 200 '("Content-type" . "application/xml"))
  (elnode-http-return httpcon (baggy/feed)))

(defun baggy-router (httpcon)
  "Baggy the blog engine's URL router."
  (let ((webserver
         (elnode-webserver-handler-maker
          baggy-root)))
    (elnode-hostpath-dispatcher
     httpcon
     `((,(format "^[^/]+//%sfeed.xml$" baggy-blog-dir) . baggy-feed)
       (,(format "^[^/]+//%s\\(.*\\)" baggy-blog-dir) . baggy-post)
       ("^[^/]+//$" . baggy-index-page)
       ("^[^/]+//\\(.*\\)" . ,webserver)))))

;;;###autoload
(defun baggy-start ()
  (interactive)
  (elnode-start 'baggy-router :port baggy-port :host baggy-host))

(provide 'baggy)

;;; baggy.el ends here
