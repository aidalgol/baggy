;;; baggy-test.el --- Baggy tests -*- lexical-binding: t -*-

;; Copyright (C) 2013  Aidan Gauland

;; Author: Aidan Gauland <aidalgol@amuri.net>
;; Keywords: hypermedia, lisp
;; Based on Skinny by Nic Ferrier

;; This program is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <http://www.gnu.org/licenses/>.

;;; Commentary:

;; ERT tests for Baggy.

;;; Code:

(require 'ert)
(require 'baggy)
(require 'creole)

(defmacro baggy-test/with-test-site (&rest body)
  "Evaluate BODY with a test Baggy site in /tmp/.

`baggy-root' is rebound to a path in /tmp/.  `post-basenames' is
bound to a list of strings foo, bar, baz, and goo, from which
corresponding .creole files are created, in that order, in
`baggy-blog-dir'."
  `(let* ((baggy-root "/tmp/baggy-test-root/")
          (post-basenames '("foo" "bar" "baz" "goo"))
          (post-timestamps '("2013-05-13T09:38:10Z" "2013-05-13T09:38:18Z" "2013-05-13T09:38:21Z" "2013-05-13T09:38:27Z")))
     (unwind-protect
         (progn
           (make-directory (concat baggy-root baggy-blog-dir) t)
           (cl-map nil
                   (lambda (post-name timestamp uuid)
                     (let ((date-dir (format-time-string "%Y/%m/" (date-to-time timestamp))))
                       ;; Make date directory.
                       (make-directory (concat baggy-root baggy-blog-dir date-dir) t)
                       ;; post contents
                       (with-temp-file (concat baggy-root baggy-blog-dir
                                     (format-time-string "%Y/%m/" (date-to-time timestamp))
                                     post-name ".creole")
                         (insert post-name))
                       ;; post metadata
                       (with-temp-file (concat baggy-root baggy-blog-dir
                                               date-dir
                                               post-name ".sexp")
                         (prin1 `((title . ,post-name)
                                  (summary . ,(concat "Dummy post " post-name))
                                  (timestamp . ,timestamp)
                                  (uuid . ,uuid))
                                (current-buffer)))))
                   post-basenames
                   post-timestamps
                   '("7a76edc6-eeb4-4c8e-af33-c75c40899b69" "de76caa3-f47c-46d9-a32a-d3d0a502f358"
                     "8f59cbc1-f27d-4904-97d4-0893da8420ea" "32fae415-f6f4-4d89-a103-f49a5fe7bfe0"))
           ,@body)
       ;; Path hard-coded here in case caller changes `baggy-root' in BODY.
       (delete-directory "/tmp/baggy-test-root/" t))))

(ert-deftest baggy/post-meta-data ()
  (let* ((metadata-string "((title . \"Lorem ipsum\")(summary . \"Lorem ipsum dolor sit amet, consectetuer adipiscing elit.\")(timestamp . \"2013-05-12T02:54:39Z\")(uuid . \"deadbeef-dead-beef-dead-beefdeadbeef\"))")
         (metadata (read metadata-string))
         (blog-dir "/path/to/blog/"))
   (fakir-mock-file
    (fakir-file
     :filename "post.sexp"
     :directory blog-dir
     :content metadata-string)
    (should (equal metadata
                   (baggy/post-meta-data (concat blog-dir "post.html"))))
    (should (equal metadata
                   (baggy/post-meta-data (concat blog-dir "post.creole")))))))

(ert-deftest baggy/list-posts ()
  (baggy-test/with-test-site
   (should (equal
            (reverse
             (cl-map 'list
              (lambda (post timestamp)
                (concat baggy-root baggy-blog-dir
                        (format-time-string "%Y/%m/" (date-to-time timestamp))
                        post "." (symbol-name baggy-post-format)))
              post-basenames
              post-timestamps))
            (baggy/list-posts)))))

(ert-deftest baggy/posts-html-list ()
  (baggy-test/with-test-site
   (should (equal
            `(ul ()
              ,@(cl-map 'list
                 (lambda (post timestamp)
                   `(li ()
                      (a ((href . ,(concat baggy-blog-dir
                                           (format-time-string "%Y/%m/" (date-to-time timestamp))
                                           post)))
                         ,post)))
                 (reverse post-basenames)
                 (reverse post-timestamps)))
            (baggy/posts-html-list)))))

(ert-deftest baggy/render-post ()
  (unwind-protect
      (let ((baggy-post-format 'html)
            (baggy-lang "en")
            (baggy-blog-author "Millicent Bystander")
            (baggy-blog-dir "blog/")
            (baggy-css-dir "css/")
            (baggy-blog-css-file-names '("blog.css"))
            (baggy-blog-article-class "blogpost")

            (tmp-dir "/tmp/baggy-test/blog/2013/05/")
            (tmp-dir-drafts "/tmp/baggy-test/blog/drafts/")
            (post-basename "blah")
            (post-title "foo bar")
            (post-content "<p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Donec hendrerit tempor tellus. Donec pretium posuere tellus. Proin quam nisl, tincidunt et, mattis eget, convallis nec, purus. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Nulla posuere. Donec vitae dolor. Nullam tristique diam non turpis. Cras placerat accumsan nulla. Nullam rutrum. Nam vestibulum accumsan nisl.</p>")
            (post-timestamp "2013-05-16T19:13:30Z"))
        (mapcar
         (lambda (dir)
           (make-directory dir t)
           (with-temp-file (concat dir post-basename ".html")
             (insert post-content))
           (with-temp-file (concat dir post-basename ".sexp")
             (insert
              (format
               "((title . %S)\n(summary . \"blah blah\")\n(timestamp . %S)\n(uuid . \"deadbeef-dead-beef-dead-beefdeadbeef\"))\n"
               post-title post-timestamp))))
         (list tmp-dir tmp-dir-drafts))
        (should
         (equal
          `(html ((lang . ,baggy-lang))
             ,(esxml-head post-title
                '(meta ((charset . "UTF-8")))
                (meta 'author baggy-blog-author)
                (base "../../../")
                (link 'alternate "application/atom+xml" "feed.xml"
                      '((title . "site feed")))
                (css-link (concat baggy-css-dir "blog.css")))
             (body ()
               (article ((class . ,baggy-blog-article-class))
                 (header ()
                   ,post-title
                   (br ())
                   (time ((datetime . ,post-timestamp))
                     ,(format-time-string "%d %B %Y, %R %Z"
                                          (date-to-time post-timestamp))))
                 ,post-content)))
          (baggy/render-post (concat tmp-dir post-basename ".html"))))
        (should
         (equal
          `(html ((lang . ,baggy-lang))
                 ,(esxml-head post-title
                    '(meta ((charset . "UTF-8")))
                    (meta 'author baggy-blog-author)
                    (base "../../")
                    (link 'alternate "application/atom+xml" "feed.xml"
                          '((title . "site feed")))
                    (css-link (concat baggy-css-dir "blog.css")))
                 (body ()
                       (article ((class . ,baggy-blog-article-class))
                                (header ()
                                        ,post-title
                                        (br ())
                                        (time ((datetime . ,post-timestamp))
                                              ,(format-time-string "%d %B %Y, %R %Z"
                                                                   (date-to-time post-timestamp))))
                                ,post-content)))
          (baggy/render-post (concat tmp-dir-drafts post-basename ".html")))))
    (delete-directory "/tmp/baggy-test/" t)))

(ert-deftest baggy/expand-index-page ()
  (baggy-test/with-test-site
   (let ((html-format-string "<html>\n<head>n<title>test index page</title>\n</head>\n<body>\n<h1>Posts</h1>\n%s\n</body>\n</head>\n")
         (index-file-name (concat baggy-root "index.html")))
     (with-temp-file index-file-name
       (insert (format html-format-string "<!--{{{posts}}}-->")))
     (should (string=
              (format html-format-string (pp-esxml-to-xml (baggy/posts-html-list)))
              (baggy/expand-index-page index-file-name))))))

(ert-deftest baggy/feed ()
  (baggy-test/with-test-site
   (let ((baggy-lang "en")
         (baggy-blog-name "ERT blog")
         (baggy-blog-author "Millicent Bystander")
         (baggy-blog-css-file-names '("blog.css"))
         (baggy-blog-article-class "blogpost"))
     (should (string=
              (concat "<?xml version=\"1.0\"?>\n"
               (pp-esxml-to-xml
                `(feed ((xmlns . "http://www.w3.org/2005/Atom")
                        (xml:lang . ,baggy-lang))
                   (title () ,baggy-blog-name)
                   (link ((href . ,(concat baggy-base-url baggy-blog-dir "feed.xml"))
                          (rel . "self")))
                   (link ((href . "./")))
                   (id () ,(concat "urn:uuid:" baggy-feed-uuid))
                   (updated () "2013-05-13T09:38:27Z")
                   (author ()
                     (name () ,baggy-blog-author))
                   (entry ()
                     (title ((type . "xhtml"))
                       (div ((xmlns . "http://www.w3.org/1999/xhtml"))
                         "goo"))
                     (author ()
                       (name () ,baggy-blog-author))
                     (link ((href . "2013/05/goo")))
                     (id () "urn:uuid:32fae415-f6f4-4d89-a103-f49a5fe7bfe0")
                     (updated () "2013-05-13T09:38:27Z")
                     (summary ((type . "xhtml"))
                       (div ((xmlns . "http://www.w3.org/1999/xhtml"))
                         "Dummy post goo")))
                   (entry ()
                     (title ((type . "xhtml"))
                       (div ((xmlns . "http://www.w3.org/1999/xhtml"))
                         "baz"))
                     (author ()
                       (name () ,baggy-blog-author))
                     (link ((href . "2013/05/baz")))
                     (id () "urn:uuid:8f59cbc1-f27d-4904-97d4-0893da8420ea")
                     (updated () "2013-05-13T09:38:21Z")
                     (summary ((type . "xhtml"))
                       (div ((xmlns . "http://www.w3.org/1999/xhtml"))
                         "Dummy post baz")))
                   (entry ()
                     (title ((type . "xhtml"))
                       (div ((xmlns . "http://www.w3.org/1999/xhtml"))
                         "bar"))
                     (author ()
                       (name () ,baggy-blog-author))
                     (link ((href . "2013/05/bar")))
                     (id () "urn:uuid:de76caa3-f47c-46d9-a32a-d3d0a502f358")
                     (updated () "2013-05-13T09:38:18Z")
                     (summary ((type . "xhtml"))
                       (div ((xmlns . "http://www.w3.org/1999/xhtml"))
                         "Dummy post bar")))
                   (entry ()
                     (title ((type . "xhtml"))
                       (div ((xmlns . "http://www.w3.org/1999/xhtml"))
                         "foo"))
                     (author ()
                       (name () ,baggy-blog-author))
                     (link ((href . "2013/05/foo")))
                     (id () "urn:uuid:7a76edc6-eeb4-4c8e-af33-c75c40899b69")
                     (updated () "2013-05-13T09:38:10Z")
                     (summary ((type . "xhtml"))
                       (div ((xmlns . "http://www.w3.org/1999/xhtml"))
                         "Dummy post foo"))))))
              (baggy/feed))))))

;;; baggy-test.el ends here
